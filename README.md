
## B1: Cài đặt Qtcreator và clone app QT 
- Cài đặt Qtcreator theo đường link: https://download.qt.io/official_releases/qt/5.12/5.12.11/qt-opensource-linux-x64-5.12.11.run
- Sau đó tiến hành thiết lập các yêu cầu theo từng phần 
- Clone App QT từ repo trên github theo đường dẫn : https://github.com/torriem/QtAgOpenGPS 
## B2: Chạy app QT 
- Mở Qtcreator và nhấn vào mục con trỏ chuột 
![Screenshot from 2021-11-12 17-05-35](https://user-images.githubusercontent.com/78577806/141449643-8eb84aa4-91a2-4764-8208-721b1c241f17.png)
- Vào mục file system tìm đường dẫn đến nơi để folder chứa app QT. Sau đó nhấn chuột phải vào folder chọn open project in tên folder 
![Screenshot from 2021-11-12 17-02-25](https://user-images.githubusercontent.com/78577806/141449963-9985301d-a988-4c7f-b21b-b15314511d75.png)
## B3: Cài các tool để tiến hành build 
- Chọn tool trên thanh công cụ > options > kits > chọn Desktop Qt ở vị trí con trỏ chuột 
![Screenshot from 2021-11-12 17-14-19](https://user-images.githubusercontent.com/78577806/141450445-cfe7ecaa-83e7-4659-ac11-c726ce1b26a2.png)
- Tiếp theo chọn Qt version cũng chọn mục ở vị trí con trỏ chuột 
![Screenshot from 2021-11-12 17-16-29](https://user-images.githubusercontent.com/78577806/141450744-818a96ee-0169-4055-8dbc-526c1cf3f07d.png)
- Sau đó để tất cả mặc định chọn apply > ok 
- Sau đó ấn run với biểu tượng tam giác 
![Screenshot from 2021-11-12 17-19-14](https://user-images.githubusercontent.com/78577806/141451068-88886221-4e7e-454d-a509-8a27994bb96f.png)
